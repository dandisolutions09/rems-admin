import React from "react";
import AgentsCards from "../components/AgentsCards";
import { useNavigate } from "react-router-dom";
import ArrowBackIosIcon from "@mui/icons-material/ArrowBackIos";


export default function MyAgentsPage() {
  const navigate = useNavigate();
  const handle_back = () => {
    navigate("/");
  };
  return (
    <>
      <div
        className="m-4 w-10 cursor-pointer hover:scale-110 duration-200 ease-linear shadow-xl px-4 py-1 rounded-lg"
        onClick={handle_back}
      >
        <ArrowBackIosIcon sx={{ fontSize: ["18px", "22px"], marginTop: 1 }} />
      </div>
      <AgentsCards />
    </>
  );
}
