import React from 'react'
import Cards from '../components/Cards'
import ChartComponent from '../components/ChartComponent';

export default function HomePage() {
  return (
    <>
      <Cards />
      <div className='flex sm:flex-col md:flex-row'>
        <ChartComponent />
        <ChartComponent />
      </div>
    </>
  );
}
