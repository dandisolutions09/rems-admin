import React from "react";
import "../styles.css";

import { Typography } from "@mui/material";

import BlocksNavigation from "../components/BlocksNavigation";

export default function BlockB() {
  return (
    <>
      <div className="grid bg-[#242333] text-gray-100 justify-center text-center h-screen">
        <div>
          <BlocksNavigation />
        </div>
        <div className="justify-center text-center m-2 py-2">
          <Typography variant="h3" component="h2">
            Block B
          </Typography>
        </div>
        <ul class="showcase">
          <li>
            <div class="seat"></div>
            <small>N/A</small>
          </li>

          <li>
            <div class="seat selected"></div>
            <small>Booked</small>
          </li>

          <li>
            <div class="seat occupied"></div>
            <small>Sold Out</small>
          </li>
        </ul>
        <div class="container">
          <div class="screen"></div>
          <div class="row">
            <div className="bg-[#65350f] h-[50px] w-[45px] m-[3px] rounded-t-lg">
              m2
            </div>
            <div class="seat">m3</div>
            <div class="seat"></div>
            <div class="seat"></div>
            <div class="seat"></div>
            <div class="seat"></div>
            <div class="seat"></div>
            <div class="seat"></div>
          </div>
          <div class="row">
            <div class="seat">G4</div>
            <div class="seat">G6</div>
            <div class="seat"></div>
            <div class="seat occupied"></div>
            <div class="seat occupied"></div>
            <div class="seat"></div>
            <div class="seat"></div>
            <div class="seat"></div>
          </div>

          <div class="row">
            <div class="seat">E1</div>
            <div class="seat">E2</div>
            <div class="seat"></div>
            <div class="seat"></div>
            <div class="seat"></div>
            <div class="seat"></div>
            <div class="seat occupied"></div>
            <div class="seat occupied"></div>
          </div>

          <div class="row">
            <div className="bg-[#65350f] h-[50px] w-[45px] m-[3px] rounded-t-lg">
              Q2
            </div>
            <div class="seat">Q3</div>
            <div class="seat"></div>
            <div class="seat"></div>
            <div class="seat"></div>
            <div class="seat"></div>
            <div class="seat"></div>
            <div class="seat"></div>
          </div>

          <div class="row">
            <div class="seat"></div>
            <div class="seat"></div>
            <div class="seat"></div>
            <div class="seat occupied"></div>
            <div class="seat occupied"></div>
            <div class="seat"></div>
            <div class="seat"></div>
            <div class="seat"></div>
          </div>

          <div class="row">
            <div class="seat"></div>
            <div class="seat"></div>
            <div class="seat"></div>
            <div class="seat"></div>
            <div class="seat occupied"></div>
            <div class="seat occupied"></div>
            <div class="seat occupied"></div>
            <div className="bg-[#65350f] h-[50px] w-[45px] m-[3px] rounded-t-lg"></div>
          </div>
        </div>
        <div className="justify-center text-center">
          <p class="text">
            You have selected <span id="count">0</span> Room for a price of{" "}
            <span id="total">0</span>milion
          </p>
        </div>
      </div>
    </>
  );
}
