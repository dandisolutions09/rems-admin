import React from "react";
import Stack from "@mui/material/Stack";
import CircularProgress from "@mui/material/CircularProgress";
import { Box, Button, Typography } from "@mui/material";
import avatar from "../assets/avatar5.jpg";
import { useNavigate } from "react-router-dom";

export default function AgentsCards() {
  const [progress, setProgress] = React.useState(0);
  React.useEffect(() => {
    const timer = setInterval(() => {
      setProgress((prevProgress) =>
        prevProgress >= 100 ? 0 : prevProgress + 10
      );
    }, 800);

    return () => {
      clearInterval(timer);
    };
  }, []);
  const navigate = useNavigate();
  const handle_back = () => {
    navigate("/agents");
  };
  return (
    <div className="flex items-start justify-center min-h-screen container mx-auto">
      <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3">
        <div className="rounded-xl shadow-2xl">
          <div className="p-5 flex flex-col">
            <div className="rounded-xl overflow-hidden">
              <img src={avatar} alt="" className="w-[100px]" />
            </div>
            <Typography sx={{ fontSize: [18, 20], fontWeight: "bold" }}>
              Nancy Nafula
            </Typography>

            <div>
              <div className="">
                <div className="flex flex-row gap-3 my-1">
                  <Typography>Properties Sold:</Typography>
                  <Typography
                    sx={{
                      color: "#000",
                      fontSize: 23,
                      borderRadius: 2,
                    }}
                  >
                    40
                  </Typography>
                </div>
                <div className="flex flex-row gap-3 my-1">
                  <Typography>Clients:</Typography>
                  <Typography
                    sx={{
                      color: "#000",
                      fontSize: 23,
                      borderRadius: 2,
                    }}
                  >
                    40
                  </Typography>
                </div>
              </div>
            </div>
            <Typography className="text-slate-500 text-lg mt-3">
              Properties Sold and general information about Nancy
            </Typography>
            <div>
              <Button
                variant="contained"
                sx={{ bgcolor: "#242333" }}
                onClick={handle_back}
                className="text-center bg-blue-400 text-blue-700 py-2 px- rounded-lg font-semibold mt-4 hover:bg-blue-300 focus:scale-95 transition-all duration-200 ease-out"
              >
                Explore
              </Button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
