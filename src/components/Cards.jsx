import { Typography } from '@mui/material';
import React from 'react'
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { MdHouse } from "react-icons/md";
import { BsCashStack } from "react-icons/bs";
import { FaPeopleArrows } from "react-icons/fa";
import { GiReceiveMoney } from "react-icons/gi";
import { FaBalanceScale } from "react-icons/fa";
import LightTooltip from './LightTooltip';

export default function Cards() {
  const navigate = useNavigate();
  const handleNavigate_Agents = () =>{
    navigate("/myagents")
  } 
  return (
    <>
      <div className="m-2">
        <div className="flex flex-col md:flex-col lg:flex-col gap-2">
          <div className="border border-gray-300 p-4 shadow-lg rounded-xl bg-[#242333] flex py-8">
            <div className="flex flex-row gap-2">
              <BsCashStack style={{ color: "white" }} />
              <div className="  md:max-w-lg">
                <Typography
                  variant="body2"
                  color="#fff"
                  style={{ fontSize: "12px" }}
                >
                  Total Property Value Sold:
                </Typography>
                <Typography
                  variant="body2"
                  color="#fff"
                  style={{ fontSize: "40px" }}
                >
                  300M
                </Typography>
              </div>
            </div>
          </div>
          {/* first leg */}
          <div className="flex flex-col md:flex-col sm:flex-col lg:flex-row gap-2 w-full">
            <div className="flex flex-row md:flex-row gap-2 w-full">
              <LightTooltip title="Number of Agents in the Project">
                <div className=" border border-gray-300 w-[280px]  md:w-[445px] lg:w-full p-4 shadow-lg rounded-xl bg-[#242333]">
                  <MdHouse style={{ color: "white", margin: "2px" }} />
                  <Typography
                    variant="body2"
                    color="#fff"
                    style={{ fontSize: "12px" }}
                  >
                    Number of Properties Sold:
                  </Typography>
                  <Typography
                    variant="body2"
                    color="white"
                    style={{ fontSize: "40px" }}
                  >
                    50
                  </Typography>
                </div>
              </LightTooltip>

              <LightTooltip title="Number of Agents in the Project">
                <div className=" border border-gray-300 w-[280px]  md:w-[445px] lg:w-full p-4 shadow-lg rounded-xl bg-[#242333]">
                  <FaPeopleArrows style={{ color: "white", margin: "2px" }} />
                  <Typography
                    variant="body2"
                    color="#fff"
                    style={{ fontSize: "12px" }}
                  >
                    Clients:
                  </Typography>
                  <Typography
                    variant="body2"
                    color="#fff"
                    style={{ fontSize: "40px" }}
                  >
                    200
                  </Typography>
                </div>
              </LightTooltip>
            </div>
            <div className="flex flex-row md:flex-row gap-2">
              <LightTooltip title="Number of Agents in the Project">
                <div className=" border border-gray-300 w-[280px]  md:w-[445px] lg:w-[350px] p-4 shadow-lg rounded-xl bg-[#242333]">
                  <GiReceiveMoney style={{ color: "white", margin: "2px" }} />
                  <Typography
                    variant="body2"
                    color="#fff"
                    style={{ fontSize: "12px" }}
                  >
                    Total Deposits
                  </Typography>
                  <Typography
                    variant="body2"
                    color="#fff"
                    style={{ fontSize: "34px" }}
                  >
                    100M
                  </Typography>
                </div>
              </LightTooltip>

              <LightTooltip title="Number of Agents in the Project">
                <div className=" border border-gray-300 w-[280px]  md:w-[445px] lg:w-[350px] p-4 shadow-lg rounded-xl bg-[#242333]">
                  <FaBalanceScale style={{ color: "white", margin: "2px" }} />
                  <Typography
                    variant="body2"
                    color="#fff"
                    style={{ fontSize: "12px" }}
                  >
                    Remaining Balance
                  </Typography>
                  <Typography
                    variant="body2"
                    color="#fff"
                    style={{ fontSize: "40px" }}
                  >
                    170M
                  </Typography>
                </div>
              </LightTooltip>
            </div>
          </div>
          <div className="flex flex-col md:flex-col sm:flex-col lg:flex-row gap-2 w-full">
            <div className="flex flex-row md:flex-row gap-2 w-full">
              <LightTooltip title="Number of Agents in the Project">
                <div className=" border border-gray-300 w-[280px]  md:w-[445px] lg:w-full p-4 shadow-lg rounded-xl bg-[#242333]">
                  <MdHouse style={{ color: "white", margin: "2px" }} />
                  <Typography
                    variant="body2"
                    color="#fff"
                    style={{ fontSize: "12px" }}
                  >
                    Number of Properties Sold:
                  </Typography>
                  <Typography
                    variant="body2"
                    color="white"
                    style={{ fontSize: "40px" }}
                  >
                    75
                  </Typography>
                </div>
              </LightTooltip>

              <LightTooltip title="Number of Agents in the Project">
                <div className="border border-gray-300 w-[280px]  md:w-[445px] lg:w-full p-4 shadow-lg rounded-xl bg-[#242333]">
                  <FaPeopleArrows style={{ color: "white", margin: "2px" }} />
                  <Typography
                    variant="body2"
                    color="#fff"
                    style={{ fontSize: "12px" }}
                  >
                    Number of Agents:
                  </Typography>
                  <Typography
                    variant="body2"
                    color="#fff"
                    style={{ fontSize: "40px" }}
                  >
                    15
                  </Typography>
                </div>
              </LightTooltip>
            </div>
            <div className="flex flex-row md:flex-row gap-2">
              <div className=" border border-gray-300 w-[280px]  md:w-[445px] lg:w-[350px] p-4 shadow-lg rounded-xl bg-[#242333]">
                <GiReceiveMoney style={{ color: "white", margin: "2px" }} />
                <Typography
                  variant="body2"
                  color="#fff"
                  style={{ fontSize: "12px" }}
                >
                  Total Deposits
                </Typography>
                <Typography
                  variant="body2"
                  color="#fff"
                  style={{ fontSize: "34px" }}
                >
                  30M
                </Typography>
              </div>
              <LightTooltip title="Number of Agents in the Project">
                <div className=" border border-gray-300 w-[280px]  md:w-[445px] lg:w-[350px] p-4 shadow-lg rounded-xl bg-[#242333]">
                  <FaBalanceScale style={{ color: "white", margin: "2px" }} />
                  <Typography
                    variant="body2"
                    color="#fff"
                    style={{ fontSize: "12px" }}
                  >
                    Remaining Balance
                  </Typography>
                  <Typography
                    variant="body2"
                    color="#fff"
                    style={{ fontSize: "40px" }}
                  >
                    17M
                  </Typography>
                </div>
              </LightTooltip>
            </div>
          </div>
          <div className="flex justify-center">
            <LightTooltip title="view your Agents">

              <div
                className="flex border md:mt-6 md:mb-6  md:w-[600px] md:flex md:justify-center items-center border-gray-300 p-4 w-1/2 md: shadow-lg rounded-xl bg-gray-400 mt-2 justify-center  text-center cursor-pointer hover:bg-opacity-50 ease-in duration-200  select-none"
                onClick={handleNavigate_Agents}
              >
                <Typography color={"#111111"} fontWeight={500}>
                  View Your Agents
                </Typography>
              </div>
            </LightTooltip>
          </div>
        </div>
      </div>
    </>
  );
}
