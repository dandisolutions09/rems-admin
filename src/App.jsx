import { useState } from 'react'
import { ThemeProvider, createTheme } from "@mui/material";
import { Routes, Route, HashRouter } from "react-router-dom";
import MainNavbar from './components/Navbar';
import Navbar from './components/Navbar';
import HomePage from './pages/HomePage';
import AgentsPage from './pages/AgentsPage';
import AgentsCards from './components/AgentsCards';
import MyAgentsPage from './pages/MyAgentsPage';


function App() {
  const theme = createTheme({
    typography: {
      fontFamily: ["Play"],
    },
  });

  return (
    <>
      <ThemeProvider theme={theme}>
        <HashRouter>
          <Navbar />
          <Routes>
            <Route index element={<HomePage />} />
            <Route path="/" element={<HomePage />} />
            <Route path="agents" element={<AgentsPage />} />
            <Route path="myagents" element={<MyAgentsPage />} />
          </Routes>
        </HashRouter>
      </ThemeProvider>
    </>
  );
}

export default App
